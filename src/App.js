import React from 'react';
import SwipeBothSide from './components/SwipeBothSide'
import SortList from './components/SortList'
import './App.css';

function App() {
  return (
    <div className="App">
     {/* <SwipeBothSide/> */}
     <SortList/>
    </div>
  );
}

export default App;
